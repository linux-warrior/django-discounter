# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime

try:
    from unittest.mock import MagicMock
except ImportError:
    from mock import MagicMock

from django.contrib.auth.models import AnonymousUser
from freezegun import freeze_time

from discounter.core.models import Product, CataloguePrice
from discounter.discounts.models import Discount
from discounter.utils.testcases import DiscounterTestCase


class DiscountTest(DiscounterTestCase):
    def test_catalogue_prices(self):
        # После создания скидок в базе должны были появиться соответствующие
        # экземпляры модели CataloguePrice.
        expected_catalogue_prices_by_discount = [
            'product-0, None: 50.00',
            'product-1, None: 100.00',
            'product-2, None: 150.00',
            'product-3, None: 200.00',
            'product-4, None: 50.00',
            'product-5, None: 100.00',
            'product-6, None: 150.00',
            'product-7, None: 200.00',

            'product-1, product_discount-0: 80.00',
            'product-3, product_discount-1: 160.00',
            'product-5, product_discount-2: 80.00',
            'product-7, product_discount-3: 160.00',

            'product-2, brand_discount-4: 127.50',
            'product-3, brand_discount-4: 170.00',
            'product-6, brand_discount-4: 127.50',
            'product-7, brand_discount-4: 170.00',

            'product-4, group_discount-5: 45.00',
            'product-5, group_discount-5: 90.00',
            'product-6, group_discount-5: 135.00',
            'product-7, group_discount-5: 180.00',
        ]

        def catalogue_price_repr(catalogue_price):
            discount_name = catalogue_price.discount.name if catalogue_price.discount_id is not None else 'None'
            return '{}, {}: {}'.format(
                catalogue_price.product.name,
                discount_name,
                catalogue_price.sale_price,
            )

        self.assertQuerysetEqual(
            CataloguePrice.objects.all(),
            expected_catalogue_prices_by_discount,
            transform=catalogue_price_repr
        )

        # Пробуем удалить все CataloguePrice и пересохранить товары. В
        # результате в базе должны появиться все те же экземпляры этой модели,
        # только по-другому отсортированные.
        expected_catalogue_prices_by_product = [
            'product-0, None: 50.00',

            'product-1, None: 100.00',
            'product-1, product_discount-0: 80.00',

            'product-2, None: 150.00',
            'product-2, brand_discount-4: 127.50',

            'product-3, None: 200.00',
            'product-3, product_discount-1: 160.00',
            'product-3, brand_discount-4: 170.00',

            'product-4, None: 50.00',
            'product-4, group_discount-5: 45.00',

            'product-5, None: 100.00',
            'product-5, product_discount-2: 80.00',
            'product-5, group_discount-5: 90.00',

            'product-6, None: 150.00',
            'product-6, brand_discount-4: 127.50',
            'product-6, group_discount-5: 135.00',

            'product-7, None: 200.00',
            'product-7, product_discount-3: 160.00',
            'product-7, brand_discount-4: 170.00',
            'product-7, group_discount-5: 180.00'
        ]
        self.assertEqual(
            set(expected_catalogue_prices_by_discount),
            set(expected_catalogue_prices_by_product)
        )

        CataloguePrice.objects.all().delete()
        for product in Product.objects.all():
            product.save()

        self.assertQuerysetEqual(
            CataloguePrice.objects.all(),
            expected_catalogue_prices_by_product,
            transform=catalogue_price_repr
        )

        # Пробуем удалить все CataloguePrice и пересохранить сначала скидки, а
        # затем товары.
        expected_catalogue_prices_by_discount_nulls_last = [
            'product-1, product_discount-0: 80.00',
            'product-3, product_discount-1: 160.00',
            'product-5, product_discount-2: 80.00',
            'product-7, product_discount-3: 160.00',

            'product-2, brand_discount-4: 127.50',
            'product-3, brand_discount-4: 170.00',
            'product-6, brand_discount-4: 127.50',
            'product-7, brand_discount-4: 170.00',

            'product-4, group_discount-5: 45.00',
            'product-5, group_discount-5: 90.00',
            'product-6, group_discount-5: 135.00',
            'product-7, group_discount-5: 180.00',

            'product-0, None: 50.00',
            'product-1, None: 100.00',
            'product-2, None: 150.00',
            'product-3, None: 200.00',
            'product-4, None: 50.00',
            'product-5, None: 100.00',
            'product-6, None: 150.00',
            'product-7, None: 200.00',
        ]
        self.assertEqual(
            set(expected_catalogue_prices_by_discount),
            set(expected_catalogue_prices_by_discount_nulls_last)
        )

        CataloguePrice.objects.all().delete()
        for discount in Discount.objects.all():
            discount.save()
        for product in Product.objects.all():
            product.save()

        self.assertQuerysetEqual(
            CataloguePrice.objects.all(),
            expected_catalogue_prices_by_discount_nulls_last,
            transform=catalogue_price_repr
        )

    def test_get_active_discounts(self):
        # Тестируем активные скидки для незарегистрированного пользователя.
        expected_active_discounts = [
            'product_discount-0: 20.00%',
            'product_discount-1: 20.00%',
            'product_discount-2: 20.00%',
            'product_discount-3: 20.00%',
            'brand_discount-4: 15.00%',
            'group_discount-5: 10.00%',
        ]

        request_mock = MagicMock()
        request_mock.user = AnonymousUser()

        def discount_repr(discount):
            return '{}: {}%'.format(discount.name, discount.percentage)

        active_discounts = Discount.get_active_discounts(request=request_mock)
        self.assertQuerysetEqual(
            active_discounts,
            expected_active_discounts,
            transform=discount_repr
        )

        # Теперь выполняем тест для пользователя, для которого действует
        # клиентская скидка.
        expected_active_discounts = [
            'product_discount-0: 20.00%',
            'product_discount-1: 20.00%',
            'product_discount-2: 20.00%',
            'product_discount-3: 20.00%',
            'brand_discount-4: 15.00%',
            'group_discount-5: 10.00%',
            'client_discount-6: 5.00%',
        ]

        request_mock.user = self.user
        active_discounts = Discount.get_active_discounts(request=request_mock)
        self.assertQuerysetEqual(
            active_discounts,
            expected_active_discounts,
            transform=discount_repr
        )

        # Проверяем действие скидок в прошлом и будущем.
        expected_active_discounts = [
            'product_discount-0: 20.00%',
            'product_discount-2: 20.00%',
            'brand_discount-4: 15.00%',
            'client_discount-6: 5.00%',
        ]

        today = datetime.date.today()
        past_date = today - datetime.timedelta(days=20)
        future_date = today + datetime.timedelta(days=20)

        for sample_date in (past_date, future_date):
            with freeze_time(sample_date):
                active_discounts = Discount.get_active_discounts(request=request_mock)
                self.assertQuerysetEqual(
                    active_discounts,
                    expected_active_discounts,
                    transform=discount_repr
                )

    def test_get_checkout_discounts(self):
        expected_checkout_discounts_dict = {
            'product-0': [
                'client_discount-6: 5.00%',
            ],
            'product-1': [
                'product_discount-0: 20.00%',
                'client_discount-6: 5.00%',
            ],
            'product-2': [
                'brand_discount-4: 15.00%',
                'client_discount-6: 5.00%',
            ],
            'product-3': [
                'product_discount-1: 20.00%',
                'brand_discount-4: 15.00%',
                'client_discount-6: 5.00%',
            ],
            'product-4': [
                'group_discount-5: 10.00%',
                'client_discount-6: 5.00%',
            ],
            'product-5': [
                'product_discount-2: 20.00%',
                'group_discount-5: 10.00%',
                'client_discount-6: 5.00%',
            ],
            'product-6': [
                'brand_discount-4: 15.00%',
                'group_discount-5: 10.00%',
                'client_discount-6: 5.00%',
            ],
            'product-7': [
                'product_discount-3: 20.00%',
                'brand_discount-4: 15.00%',
                'group_discount-5: 10.00%',
                'client_discount-6: 5.00%'
            ],
        }

        request_mock = MagicMock()
        request_mock.user = self.user

        def discount_repr(discount):
            return '{}: {}%'.format(discount.name, discount.percentage)

        for product in Product.objects.all():
            checkout_discounts = Discount.get_checkout_discounts(product, request=request_mock)
            expected_checkout_discounts = expected_checkout_discounts_dict[product.name]
            self.assertQuerysetEqual(
                checkout_discounts,
                expected_checkout_discounts,
                transform=discount_repr
            )

    def test_best_discount(self):
        expected_best_discounts_dict = {
            'product-0': 'client_discount-6: 5.00%',
            'product-1': 'product_discount-0: 20.00%',
            'product-2': 'brand_discount-4: 15.00%',
            'product-3': 'product_discount-1: 20.00%',
            'product-4': 'group_discount-5: 10.00%',
            'product-5': 'product_discount-2: 20.00%',
            'product-6': 'brand_discount-4: 15.00%',
            'product-7': 'product_discount-3: 20.00%',
        }

        request_mock = MagicMock()
        request_mock.user = self.user

        def discount_repr(discount):
            return '{}: {}%'.format(discount.name, discount.percentage)

        for product in Product.objects.all():
            best_discount = Discount.best_discount(product, request=request_mock)
            expected_best_discount = expected_best_discounts_dict[product.name]
            self.assertEqual(discount_repr(best_discount), expected_best_discount)
