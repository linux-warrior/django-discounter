# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime
from decimal import Decimal

import factory


class DiscountFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'discounts.Discount'
        exclude = ('today',)

    name = factory.Sequence(lambda n: 'discount-{}'.format(n))
    percentage = Decimal('10')
    today = factory.LazyAttribute(lambda obj: datetime.date.today())

    @factory.lazy_attribute_sequence
    def valid_from(self, n):
        if n % 2 == 0:
            return None
        else:
            return self.today - datetime.timedelta(days=10)

    @factory.lazy_attribute_sequence
    def valid_to(self, n):
        if n % 2 == 0:
            return None
        else:
            return self.today + datetime.timedelta(days=10)


class ProductDiscountFactory(DiscountFactory):
    class Meta:
        model = 'discounts.ProductDiscount'

    name = factory.Sequence(lambda n: 'product_discount-{}'.format(n))
    percentage = Decimal('20')


class BrandDiscountFactory(DiscountFactory):
    class Meta:
        model = 'discounts.BrandDiscount'

    name = factory.Sequence(lambda n: 'brand_discount-{}'.format(n))
    percentage = Decimal('15')


class GroupDiscountFactory(DiscountFactory):
    class Meta:
        model = 'discounts.GroupDiscount'

    name = factory.Sequence(lambda n: 'group_discount-{}'.format(n))
    percentage = Decimal('10')


class ClientDiscountFactory(DiscountFactory):
    class Meta:
        model = 'discounts.ClientDiscount'

    name = factory.Sequence(lambda n: 'client_discount-{}'.format(n))
    percentage = Decimal('5')
