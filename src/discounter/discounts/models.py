# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime
from functools import reduce
import operator

from django.conf import settings
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.db.models import Q
from django.utils.encoding import python_2_unicode_compatible
from polymorphic import PolymorphicModel

from discounter.core.models import Product, CataloguePrice
from discounter.utils.helpers import find_subclasses


@python_2_unicode_compatible
class Discount(PolymorphicModel):
    """Базовая модель для скидок."""
    name = models.CharField(verbose_name='название', max_length=256, unique=True)
    is_active = models.BooleanField('действующая?', default=True)
    valid_from = models.DateField(verbose_name='дата начала действия', blank=True, null=True, db_index=True)
    valid_to = models.DateField(verbose_name='дата окончания действия', blank=True, null=True, db_index=True)
    percentage = models.DecimalField(
        verbose_name='скидка, %',
        max_digits=4,
        decimal_places=2,
        validators=[MinValueValidator(0), MaxValueValidator(100)]
    )

    class Meta:
        verbose_name = 'скидка'
        verbose_name_plural = 'скидки'
        ordering=['pk']

    def __str__(self):
        return '{name}: {percentage}%'.format(name=self.name, percentage=self.percentage)

    def save(self, *args, **kwargs):
        super(Discount, self).save(*args, **kwargs)
        self._update_catalogue_prices()

    def _update_catalogue_prices(self):
        """Обновляем цены со скидкой для отображения в каталоге."""
        for product in self.get_catalogue_products():
            sale_price = self.get_sale_price(product)
            CataloguePrice.objects.update_or_create(
                product=product,
                discount=self,
                unique_product_id=None,
                defaults={'sale_price': sale_price}
            )

    def get_sale_price(self, product):
        """Рассчитывает цену со скидкой."""
        return product.price * (100 - self.percentage) / 100

    # Отображается ли этот класс скидкок в каталоге.
    displayed_in_catalogue = True

    # Фильтр, возвращающий пустое множество объектов.
    FILTER_NONE = object()
    # Фильтр, возвращающий все объекты.
    FILTER_ALL = object()

    def _products_filter(self):
        return self.FILTER_NONE

    def get_catalogue_products(self):
        """Возвращает товары, которые отображаются в каталоге с этой скидкой."""
        if self.displayed_in_catalogue:
            products_filter = self._products_filter()
        else:
            products_filter = self.FILTER_NONE
        if products_filter is self.FILTER_NONE:
            return Product.objects.none()

        if products_filter is self.FILTER_ALL:
            products_filter = Q()

        return Product.objects.filter(products_filter)

    @classmethod
    def subtree_iter(cls):
        yield cls
        for descendant_cls in find_subclasses(cls):
            yield descendant_cls

    @classmethod
    def _discounts_filter(cls, product):
        return cls.FILTER_NONE

    @classmethod
    def _subtree_discounts_filter(cls, product, catalogue_only=True):
        filters_list = []
        for node_cls in cls.subtree_iter():
            if not catalogue_only or node_cls.displayed_in_catalogue:
                discounts_filter = node_cls._discounts_filter(product)
            else:
                discounts_filter = cls.FILTER_NONE
            if discounts_filter is cls.FILTER_NONE:
                discounts_filter = Q()
            elif discounts_filter is cls.FILTER_ALL:
                discounts_filter = Q(**{'{}___pk__isnull'.format(node_cls.__name__): False})
            filters_list.append(discounts_filter)

        if not any(filters_list):
            return cls.FILTER_NONE

        return reduce(operator.__ior__, filters_list)

    @classmethod
    def get_catalogue_discounts(cls, product):
        """Возвращает скидки на товар, которые отображаются в каталоге."""
        discounts_filter = cls._subtree_discounts_filter(product, catalogue_only=True)
        if discounts_filter is cls.FILTER_NONE:
            return cls.objects.none()

        return cls.objects.filter(discounts_filter)

    @classmethod
    def _valid_filter(cls):
        today = datetime.date.today()
        valid_from = Q(valid_from__isnull=True) | Q(valid_from__lte=today)
        valid_to = Q(valid_to__isnull=True) | Q(valid_to__gte=today)
        return valid_from & valid_to & Q(is_active=True)

    @classmethod
    def _active_filter(cls, **kwargs):
        return cls.FILTER_NONE

    @classmethod
    def _subtree_active_filter(cls, **kwargs):
        filters_list = []
        for node_cls in cls.subtree_iter():
            active_filter = node_cls._active_filter(**kwargs)
            if active_filter is cls.FILTER_NONE:
                active_filter = Q()
            elif active_filter is cls.FILTER_ALL:
                active_filter = Q(**{'{}___pk__isnull'.format(node_cls.__name__): False})
            filters_list.append(active_filter)

        if not any(filters_list):
            return cls.FILTER_NONE

        return reduce(operator.__ior__, filters_list)

    @classmethod
    def get_active_discounts(cls, **kwargs):
        """Возвращает все действующие скидки, вне зависимости от товара."""
        active_filter = cls._subtree_active_filter(**kwargs)
        if active_filter is cls.FILTER_NONE:
            return cls.objects.none()

        valid_filter = cls._valid_filter()
        return cls.objects.filter(valid_filter, active_filter)

    @classmethod
    def get_checkout_discounts(cls, product, **kwargs):
        """Возвращает скидки на товар, которые действуют при заказе."""
        discounts_filter = cls._subtree_discounts_filter(product, catalogue_only=False)
        if discounts_filter is cls.FILTER_NONE:
            return cls.objects.none()

        return cls.get_active_discounts(**kwargs).filter(discounts_filter)

    @classmethod
    def best_discount(cls, product, **kwargs):
        """Находит самую лучшую скидку."""
        discounts_list = list(cls.get_checkout_discounts(product, **kwargs))
        if not discounts_list:
            return None

        return min(discounts_list, key=lambda discount: discount.get_sale_price(product))


class ProductDiscount(Discount):
    product = models.ForeignKey('core.Product', verbose_name='товар')

    class Meta:
        verbose_name = 'скидка на товар'
        verbose_name_plural = 'скидки на товары'

    def _products_filter(self):
        return Q(pk=self.product_id)

    @classmethod
    def _discounts_filter(cls, product):
        if product.pk is None:
            return cls.FILTER_NONE

        return Q(ProductDiscount___product=product.pk)

    @classmethod
    def _active_filter(cls, **kwargs):
        return cls.FILTER_ALL


class BrandDiscount(Discount):
    brand = models.ForeignKey('core.Brand', verbose_name='бренд')

    class Meta:
        verbose_name = 'скидка на бренд'
        verbose_name_plural = 'скидки на бренды'

    def _products_filter(self):
        return Q(brand=self.brand_id)

    @classmethod
    def _discounts_filter(cls, product):
        if product.brand_id is None:
            return cls.FILTER_NONE

        return Q(BrandDiscount___brand=product.brand_id)

    @classmethod
    def _active_filter(cls, **kwargs):
        return cls.FILTER_ALL


class GroupDiscount(Discount):
    group = models.ForeignKey('core.Group', verbose_name='группа товаров')

    class Meta:
        verbose_name = 'скидка на группу товаров'
        verbose_name_plural = 'скидки на группы товаров'

    def _products_filter(self):
        return Q(groups=self.group_id)

    @classmethod
    def _discounts_filter(cls, product):
        if product.pk is None:
            return cls.FILTER_NONE

        return Q(GroupDiscount___group__products=product.pk)

    @classmethod
    def _active_filter(cls, **kwargs):
        return cls.FILTER_ALL


class ClientDiscount(Discount):
    client = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='клиент')

    class Meta:
        verbose_name = 'скидка клиента'
        verbose_name_plural = 'скидки клиентов'

    displayed_in_catalogue = False

    def _products_filter(self):
        return self.FILTER_ALL

    @classmethod
    def _discounts_filter(cls, product):
        return cls.FILTER_ALL

    @classmethod
    def _active_filter(cls, **kwargs):
        request = kwargs.pop('request', None)
        if request is None or request.user.is_anonymous():
            return cls.FILTER_NONE

        return Q(ClientDiscount___client=request.user)
