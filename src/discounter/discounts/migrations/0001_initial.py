# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Discount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=256, verbose_name='\u043d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('is_active', models.BooleanField(default=True, verbose_name='\u0434\u0435\u0439\u0441\u0442\u0432\u0443\u044e\u0449\u0430\u044f?')),
                ('valid_from', models.DateField(db_index=True, null=True, verbose_name='\u0434\u0430\u0442\u0430 \u043d\u0430\u0447\u0430\u043b\u0430 \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u044f', blank=True)),
                ('valid_to', models.DateField(db_index=True, null=True, verbose_name='\u0434\u0430\u0442\u0430 \u043e\u043a\u043e\u043d\u0447\u0430\u043d\u0438\u044f \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u044f', blank=True)),
                ('percentage', models.DecimalField(verbose_name='\u0441\u043a\u0438\u0434\u043a\u0430, %', max_digits=4, decimal_places=2, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)])),
            ],
            options={
                'ordering': ['pk'],
                'verbose_name': '\u0441\u043a\u0438\u0434\u043a\u0430',
                'verbose_name_plural': '\u0441\u043a\u0438\u0434\u043a\u0438',
            },
        ),
        migrations.CreateModel(
            name='BrandDiscount',
            fields=[
                ('discount_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='discounts.Discount')),
                ('brand', models.ForeignKey(verbose_name='\u0431\u0440\u0435\u043d\u0434', to='core.Brand')),
            ],
            options={
                'verbose_name': '\u0441\u043a\u0438\u0434\u043a\u0430 \u043d\u0430 \u0431\u0440\u0435\u043d\u0434',
                'verbose_name_plural': '\u0441\u043a\u0438\u0434\u043a\u0438 \u043d\u0430 \u0431\u0440\u0435\u043d\u0434\u044b',
            },
            bases=('discounts.discount',),
        ),
        migrations.CreateModel(
            name='ClientDiscount',
            fields=[
                ('discount_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='discounts.Discount')),
                ('client', models.ForeignKey(verbose_name='\u043a\u043b\u0438\u0435\u043d\u0442', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': '\u0441\u043a\u0438\u0434\u043a\u0430 \u043a\u043b\u0438\u0435\u043d\u0442\u0430',
                'verbose_name_plural': '\u0441\u043a\u0438\u0434\u043a\u0438 \u043a\u043b\u0438\u0435\u043d\u0442\u043e\u0432',
            },
            bases=('discounts.discount',),
        ),
        migrations.CreateModel(
            name='GroupDiscount',
            fields=[
                ('discount_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='discounts.Discount')),
                ('group', models.ForeignKey(verbose_name='\u0433\u0440\u0443\u043f\u043f\u0430 \u0442\u043e\u0432\u0430\u0440\u043e\u0432', to='core.Group')),
            ],
            options={
                'verbose_name': '\u0441\u043a\u0438\u0434\u043a\u0430 \u043d\u0430 \u0433\u0440\u0443\u043f\u043f\u0443 \u0442\u043e\u0432\u0430\u0440\u043e\u0432',
                'verbose_name_plural': '\u0441\u043a\u0438\u0434\u043a\u0438 \u043d\u0430 \u0433\u0440\u0443\u043f\u043f\u044b \u0442\u043e\u0432\u0430\u0440\u043e\u0432',
            },
            bases=('discounts.discount',),
        ),
        migrations.CreateModel(
            name='ProductDiscount',
            fields=[
                ('discount_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='discounts.Discount')),
                ('product', models.ForeignKey(verbose_name='\u0442\u043e\u0432\u0430\u0440', to='core.Product')),
            ],
            options={
                'verbose_name': '\u0441\u043a\u0438\u0434\u043a\u0430 \u043d\u0430 \u0442\u043e\u0432\u0430\u0440',
                'verbose_name_plural': '\u0441\u043a\u0438\u0434\u043a\u0438 \u043d\u0430 \u0442\u043e\u0432\u0430\u0440\u044b',
            },
            bases=('discounts.discount',),
        ),
        migrations.AddField(
            model_name='discount',
            name='polymorphic_ctype',
            field=models.ForeignKey(related_name='polymorphic_discounts.discount_set+', editable=False, to='contenttypes.ContentType', null=True),
        ),
    ]
