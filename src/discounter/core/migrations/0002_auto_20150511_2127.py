# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
        ('discounts', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='catalogueprice',
            name='discount',
            field=models.ForeignKey(related_name='catalogue_prices', verbose_name='\u0441\u043a\u0438\u0434\u043a\u0430', blank=True, to='discounts.Discount', null=True),
        ),
        migrations.AddField(
            model_name='catalogueprice',
            name='product',
            field=models.ForeignKey(related_name='catalogue_prices', verbose_name='\u0442\u043e\u0432\u0430\u0440', to='core.Product'),
        ),
        migrations.AlterUniqueTogether(
            name='catalogueprice',
            unique_together=set([('product', 'discount')]),
        ),
    ]
