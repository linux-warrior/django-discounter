# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20150511_2127'),
    ]

    operations = [
        migrations.AddField(
            model_name='catalogueprice',
            name='unique_product_id',
            field=models.IntegerField(unique=True, null=True, editable=False, blank=True),
        ),
    ]
