# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Brand',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=256, verbose_name='\u043d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': '\u0431\u0440\u0435\u043d\u0434',
                'verbose_name_plural': '\u0431\u0440\u0435\u043d\u0434\u044b',
            },
        ),
        migrations.CreateModel(
            name='CataloguePrice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sale_price', models.DecimalField(verbose_name='\u0446\u0435\u043d\u0430 (\u0441\u043e \u0441\u043a\u0438\u0434\u043a\u043e\u0439)', max_digits=8, decimal_places=2, db_index=True)),
            ],
            options={
                'ordering': ['pk'],
            },
        ),
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=256, verbose_name='\u043d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': '\u0433\u0440\u0443\u043f\u043f\u0430',
                'verbose_name_plural': '\u0433\u0440\u0443\u043f\u043f\u044b',
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=256, verbose_name='\u043d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('price', models.DecimalField(verbose_name='\u0446\u0435\u043d\u0430 (\u0431\u0435\u0437 \u0441\u043a\u0438\u0434\u043a\u0438)', max_digits=8, decimal_places=2, db_index=True)),
                ('brand', models.ForeignKey(related_name='products', verbose_name='\u0431\u0440\u0435\u043d\u0434', blank=True, to='core.Brand', null=True)),
                ('groups', models.ManyToManyField(related_name='products', verbose_name='\u0433\u0440\u0443\u043f\u043f\u044b', to='core.Group')),
            ],
            options={
                'ordering': ['pk'],
                'verbose_name': '\u0442\u043e\u0432\u0430\u0440',
                'verbose_name_plural': '\u0442\u043e\u0432\u0430\u0440\u044b',
            },
        ),
    ]
