# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from decimal import Decimal

import factory


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'auth.User'
        django_get_or_create = ('username',)

    username = factory.Sequence(lambda n: 'user-{}'.format(n))
    password = factory.PostGenerationMethodCall('set_password', 'password')


class BrandFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'core.Brand'

    name = factory.Sequence(lambda n: 'brand-{}'.format(n))


class GroupFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'core.Group'

    name = factory.Sequence(lambda n: 'group-{}'.format(n))


class ProductFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'core.Product'

    name = factory.Sequence(lambda n: 'product-{}'.format(n))

    @factory.iterator
    def price():
        for n in range(1, 5):
            yield n * Decimal('50')

    @factory.post_generation
    def groups(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            self.groups = list(extracted)
