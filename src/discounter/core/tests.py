# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime

try:
    from unittest.mock import MagicMock
except ImportError:
    from mock import MagicMock

from freezegun import freeze_time

from discounter.core.models import Product
from discounter.utils.testcases import DiscounterTestCase


class ProductTest(DiscounterTestCase):
    def test_manager_with_discounts(self):
        # Проверяем цены на товары в момент времени,
        # когда действуют все скидки.
        expected_products_with_discounts = [
            'product-4: 50.00 / 45.00',
            'product-0: 50.00 / 50.00',
            'product-1: 100.00 / 80.00',
            'product-5: 100.00 / 80.00',
            'product-2: 150.00 / 127.50',
            'product-6: 150.00 / 127.50',
            'product-3: 200.00 / 160.00',
            'product-7: 200.00 / 160.00',
        ]

        request_mock = MagicMock()
        request_mock.user = self.user

        def product_repr(product):
            return '{}: {} / {}'.format(product.name, product.price, product.sale_price)

        products_with_discounts = Product.objects.with_discounts(request_mock)
        self.assertEqual(len(products_with_discounts), Product.objects.count())
        self.assertQuerysetEqual(
            products_with_discounts,
            expected_products_with_discounts,
            transform=product_repr
        )

        # Проверяем цены на товары в прошлом и будущем.
        expected_products_with_discounts = [
            'product-0: 50.00 / 50.00',
            'product-4: 50.00 / 50.00',
            'product-1: 100.00 / 80.00',
            'product-5: 100.00 / 80.00',
            'product-2: 150.00 / 127.50',
            'product-6: 150.00 / 127.50',
            'product-3: 200.00 / 170.00',
            'product-7: 200.00 / 170.00',
        ]

        today = datetime.date.today()
        past_date = today - datetime.timedelta(days=20)
        future_date = today + datetime.timedelta(days=20)

        for sample_date in (past_date, future_date):
            with freeze_time(sample_date):
                products_with_discounts = Product.objects.with_discounts(request_mock)
                self.assertEqual(len(products_with_discounts), Product.objects.count())
                self.assertQuerysetEqual(
                    products_with_discounts,
                    expected_products_with_discounts,
                    transform=product_repr
                )
