# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.db.models import Q, Min
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class Brand(models.Model):
    name = models.CharField(verbose_name='название', max_length=256, unique=True)

    class Meta:
        verbose_name = 'бренд'
        verbose_name_plural = 'бренды'

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Group(models.Model):
    name = models.CharField(verbose_name='название', max_length=256, unique=True)

    class Meta:
        verbose_name = 'группа'
        verbose_name_plural = 'группы'

    def __str__(self):
        return self.name


class ProductManager(models.Manager):
    def with_discounts(self, request):
        """Формирует список товаров для отображения в каталоге.

        Товары будут отсортированы по возрастанию цены со скидкой.

        """
        from discounter.discounts.models import Discount

        active_discounts = Discount.get_active_discounts(request=request)
        products_with_discounts = self.filter(
            Q(catalogue_prices__discount__isnull=True) | Q(catalogue_prices__discount__in=active_discounts)
        ).annotate(
            sale_price=Min('catalogue_prices__sale_price')
        ).order_by('sale_price', 'pk')

        return products_with_discounts


@python_2_unicode_compatible
class Product(models.Model):
    name = models.CharField(verbose_name='название', max_length=256, unique=True)
    price = models.DecimalField(verbose_name='цена (без скидки)', max_digits=8, decimal_places=2, db_index=True)
    brand = models.ForeignKey('Brand', verbose_name='бренд', related_name='products', blank=True, null=True)
    groups = models.ManyToManyField('Group', verbose_name='группы', related_name='products')

    objects = ProductManager()

    class Meta:
        verbose_name = 'товар'
        verbose_name_plural = 'товары'
        ordering = ['pk']

    def __str__(self):
        return '{name}: {price}'.format(name=self.name, price=self.price)

    def save(self, *args, **kwargs):
        super(Product, self).save(*args, **kwargs)
        self._update_catalogue_prices()

    def _update_catalogue_prices(self):
        """Обновляем цены со скидкой для отображения в каталоге."""
        from discounter.discounts.models import Discount

        # Дополнительно создаем запись, содержащую цену без скидки,
        # чтобы обойти ограничения Django ORM при выполнении запросов.
        CataloguePrice.objects.update_or_create(
            product=self,
            discount=None,
            unique_product_id=self.pk,
            defaults={'sale_price': self.price}
        )

        for discount in Discount.get_catalogue_discounts(self):
            sale_price = discount.get_sale_price(self)
            CataloguePrice.objects.update_or_create(
                product=self,
                discount=discount,
                unique_product_id=None,
                defaults={'sale_price': sale_price}
            )


@python_2_unicode_compatible
class CataloguePrice(models.Model):
    """Цена, которая будет отображаться в каталоге."""
    product = models.ForeignKey('Product', verbose_name='товар', related_name='catalogue_prices')
    discount = models.ForeignKey(
        'discounts.Discount',
        verbose_name='скидка',
        related_name='catalogue_prices',
        blank=True,
        null=True
    )
    # Это поле нужно для того, чтобы исключить возможность появления в
    # результате работы update_or_create() сразу нескольких записей с
    # discount = None для одного товара.
    unique_product_id = models.IntegerField(blank=True, null=True, unique=True, editable=False)
    sale_price = models.DecimalField(verbose_name='цена (со скидкой)', max_digits=8, decimal_places=2, db_index=True)

    class Meta:
        unique_together = ('product', 'discount')
        ordering = ['pk']

    def __str__(self):
        discount_name = self.discount.name if self.discount_id is not None else 'None'
        return '{product_name} ({discount_name}): {sale_price}'.format(
            product_name=self.product.name,
            discount_name=discount_name,
            sale_price=self.sale_price
        )
