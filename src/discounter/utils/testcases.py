# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase

from discounter.core.factories import (
    UserFactory, BrandFactory, GroupFactory, ProductFactory
)
from discounter.discounts.factories import (
    DiscountFactory, ProductDiscountFactory, BrandDiscountFactory, GroupDiscountFactory, ClientDiscountFactory
)


class DiscounterTestCase(TestCase):
    """Создает в базе несколько товаров и скидок."""

    def setUp(self):
        super(DiscounterTestCase, self).setUp()

        for factory_cls in (UserFactory, BrandFactory, GroupFactory, ProductFactory, DiscountFactory):
            factory_cls.reset_sequence(0)

        self.user = UserFactory()
        brand = BrandFactory()
        group = GroupFactory()

        products_params_list = [
            dict(),
            dict(brand=brand),
            dict(groups=[group]),
            dict(brand=brand, groups=[group])
        ]
        products_list = []
        for product_params in products_params_list:
            for _i in range(2):
                products_list.append(ProductFactory(**product_params))

        for i in range(1, len(products_list), 2):
            ProductDiscountFactory(product=products_list[i])
        BrandDiscountFactory(brand=brand)
        GroupDiscountFactory(group=group)
        ClientDiscountFactory(client=self.user)
