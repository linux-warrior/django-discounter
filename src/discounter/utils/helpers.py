# -*- coding: utf-8 -*-
from __future__ import unicode_literals


def find_subclasses(cls):
    if not isinstance(cls, type):
        raise TypeError("{} не является new-style классом.".format(cls))

    children_set = set()
    parents_queue = [cls]
    while parents_queue:
        parent = parents_queue.pop(0)
        for child in parent.__subclasses__():
            if child not in children_set:
                children_set.add(child)
                parents_queue.append(child)
                yield child
